
import akka.actor._
import com.github.sstone.amqp._
import Amqp._
import com.rabbitmq.client.ConnectionFactory
import scala.concurrent.duration._

object AMQPSupport {
  val queueName = "acker_queue"
}

trait AMQPSupport { self: ActorSystem ⇒

  import AMQPSupport._

  def entry: ActorRef

  val factory = new ConnectionFactory()
  factory.setUsername("guest")
  factory.setPassword("guest")
  factory.setHost("localhost")
  //create connection actor to amqp
  require(system != null, "Actor system has not been initialized")
  val conn = system.actorOf(ConnectionOwner.props(factory, 1 second))
  //create actor ref to move mq messages into akka
  val listener = system.actorOf(Props(classOf[AMQPListener], entry))
  //create AMQP router to local listener
  val consumer = ConnectionOwner.createChildActor(conn, Consumer.props(listener, channelParams = None, autoack = false))

  init()

  def init() {
    //wait for connection established
    Amqp.waitForConnection(system, consumer).await()

    val queueParams = QueueParameters(queueName, passive = false, durable = false, exclusive = false, autodelete = true)
    consumer ! DeclareQueue(queueParams)
    consumer ! QueueBind(queue = queueName, exchange = "amq.direct", routing_key = "my_key")
    consumer ! AddQueue(QueueParameters(name = queueName, passive = false))

  }
}

class AMQPListener(next: ActorRef) extends Actor with Logging {

  def receive = {
    case d @ Delivery(consumerTag, envelope, properties, body) ⇒ {
      logger.debug("Received AMQP message: " + d)
      sender ! Ack(envelope.getDeliveryTag)
    }
  }
}
