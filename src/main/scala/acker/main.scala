

import akka.actor._

import scala.io._

object Acker extends App with ActorSystem with AMQPSupport {
  val sink = system.actorOf(Props(new SimpleActor({ case _ => () })))
  val logger = system.actorOf(Props(classOf[LogMessage], sink))
  val counter = system.actorOf(Props(classOf[NodeCounter], logger))
  val identifier = system.actorOf(Props(classOf[IdentifySource], counter))
  val entry = system.actorOf(Props(classOf[ParseMessage], identifier))

  readLine
  system.shutdown
}

trait ActorSystem {

  implicit val system = ActorSystem("Acker")
}
