

import com.typesafe.scalalogging._
import org.slf4j.LoggerFactory

import akka.actor._
import Actor.Receive

trait Logging {

  val logger = Logger(LoggerFactory.getLogger(getClass))
}

class SimpleActor(receiver: Receive) extends Actor {
  override def receive: Receive = receiver
}
