
import akka.actor._

import org.json4s._

import java.io.ByteArrayInputStream
import scala.util._
import scala.io.Source

/**
 *
 */
class ParseMessage(next: ActorRef) extends Actor {
  import org.json4s.jackson._
  
  def receive = {
    case msg @ ApiPayload(name, body) ⇒ parse(name, body) map (next !) getOrElse failed(msg)
    case msg @ RawApiPayload(name, body) => parse(name, new ByteArrayInputStream(body)) map (next !) getOrElse failed(msg)
    case other ⇒ println(other); sender ! ("WTF?" -> other)
  }

  def parse(name: String, input: JsonInput): Option[ApiJsonMessage] = JsonMethods.parseOpt(input).collect {
    case obj: JObject ⇒ ApiJsonMessage(name, obj)
  }

  def failed(msg: Any) {
    sender ! ("Body of message could not be parsed as JSON" -> msg)
  }
}

/**
 *
 */
class IdentifySource(next: ActorRef) extends Actor {

  def receive = {
    case msg: ApiJsonMessage ⇒ next ! getSource(msg)
  }

  def getSource(msg: ApiJsonMessage): Sourced[ApiJsonMessage] = {
    val clusterId = getLong(msg.body, "clusterId")
    val customerId = getLong(msg.body, "customerId")
    val source = ApiMessageSource(clusterId, customerId)
    Sourced(source, msg)
  }

  def getLong(json: JObject, node: String): Option[Long] = Option(json \ node).collect { case JInt(id) ⇒ id.longValue }

}

/**
 *
 */
class NodeCounter(next: ActorRef) extends Actor {

  def receive = {
    case srcd @ Sourced(_, ApiJsonMessage(name, body)) ⇒ next ! Counted(count(body), srcd)
    case Sourced(_, _) ⇒ //unsupported
    case other ⇒ sender ! ("Unsupported message" -> other)
  }

  def count: JValue ⇒ Int = {
    case JObject(fields) ⇒ countSeq(fields map { case (name, value) ⇒ value }) + 1
    case JArray(entries) ⇒ countSeq(entries) + 1
    case JInt(_) | JString(_) | JBool(_) | JDouble(_) | JDecimal(_) | JNull ⇒ 1
    case _ ⇒ 0
  }

  def countSeq(values: Seq[JValue]): Int = values.foldLeft(0) { _ + count(_) }
}

/**
 *
 */
class LogMessage(next: ActorRef) extends Actor with Logging {

  def receive = {
    case Sourced(ApiMessageSource(clusterId, customerId), ApiJsonMessage(name, body)) ⇒ {
      val msg = s"Message [$name] received from customer [$customerId] for cluster [$clusterId]"
      println(msg)
      logger.info(msg)
      logger.debug(body.toString)
    }
    case Counted(count, Sourced(ApiMessageSource(clusterId, customerId), ApiJsonMessage(name, body))) ⇒ {
      val msg = s"Received api message [$name] with $count nodes from customer [$customerId] for cluster [$clusterId]"
      println(msg)
      logger.info(msg)
      logger.debug(body.toString)
    }
    case other ⇒ logger.info("Unknown message received:\n" + other)
  }

}

//// Message Types
case class RawApiPayload(apiName: String, body: Array[Byte])
case class ApiPayload(apiName: String, body: String)
case class ApiJsonMessage(apiName: String, body: JObject)
case class Sourced[T](source: ApiMessageSource, value: T)
case class Counted[T](count: Int, value: T)

// Clearly I don't know what our data looks like
case class ApiMessageSource(clusterId: Option[Long], customerId: Option[Long])
