name := "acker"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.11.6"

def akka(artifactId: String) = "com.typesafe.akka" %% s"akka-$artifactId" % "2.3.9"

libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-jackson" % "3.2.11",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  akka("actor"),
  akka("slf4j"),
  "com.github.sstone" %% "amqp-client" % "1.4",
  "ch.qos.logback" % "logback-classic" % "1.1.2"
)

//updateOptions := updateOptions.value.withCachedResolution(true)

scalacOptions := Seq("higherKinds", "postfixOps", "implicitConversions").map { "-language:" + _ } ++ Seq(
  "-Xlint", "-feature"
)
